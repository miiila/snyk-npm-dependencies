var app = new Vue({
  el: '#app',
  data: {
    packages: [],
    selectedPackage: null,
    error: null,
    warning: null,
    isLoading: false,
    packageInfo: null
  },
  methods: {
    loadPackages: function() {
      axios
        .get(`http://localhost:3000/packages`)
        .then(response => {
          this.packages = response.data;
        })
        .catch(e => {
          this.error = e.message;
        });
    },
    submitForm: function() {
      this.error = null;
      if (!this.selectedPackage) {
        return;
      } else if (/\s/.test(this.selectedPackage)) {
        this.warning = 'Please provide one package only with `@` before a version.';
        setTimeout(() => {
          this.warning = null;
        }, 3000);
        return;
      }
      this.isLoading = true;
      axios
        .get(`http://localhost:3000/package/${this.selectedPackage}`)
        .then(response => {
          this.packageInfo = {
            name: this.selectedPackage,
            children: response.data
          };
          this.isLoading = false;
        })
        .catch(e => {
          console.log(e);
          this.error = e.message;
          this.isLoading = false;
        });
    }
  }
});
