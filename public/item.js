// https://vuejs.org/v2/examples/tree-view.html
Vue.component('item', {
  template: `
        <li>
          <div
            @click="toggle">
            {{model.name || model}}
            <span v-if="isFolder">[{{open ? '-' : '+'}}]</span>
          </div>
          <ul v-show="open" v-if="isFolder" class='menu-list'>
            <item
              class="item"
              v-for="(version, name) in model.children"
              :key=version
              :model='name+"@"+version'>
            </item>
            <span v-if=!Object.keys(model.children).length>No dependencies.</span>
          </ul>
        </li>
      `,
  props: {
    model: {}
  },
  data: function() {
    return {
      open: false
    };
  },
  computed: {
    isFolder: function() {
      return this.model.children;
    }
  },
  methods: {
    toggle: function() {
      if (this.isFolder) {
        this.open = !this.open;
      }
    }
  }
});
