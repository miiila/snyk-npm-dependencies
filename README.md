# snyk-npm-dependencies
Small web application for obtaining NPM dependencies. Application has a frontend part (running on localhost:3000) and backend API. Only **first-level** dependencies are shown in a way, how they are listed in package json.

Application is caching results for specific versions (e.g 2.4.0), wildcard results and tags are not cached.

## Installation

```sh
$ git clone miiila@bitbucket.org:miiila/snyk-npm-dependencies.git
$ npm install
```

## Usage

```sh
$ node index.js
```
## License

MIT © [miiila]()
