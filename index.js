const express = require('express');
const NodeCache = require('node-cache');
const request = require('request');

const app = express();
const cache = new NodeCache();

app.use(express.static('public'));

app.get('/', function(req, res) {
  res.sendFile(`${__dirname}/public/index.html`);
});

app.get('/packages', function(req, res) {
  cache.keys((err, packages) => {
    if (err) {
      console.error(err);
      return res.sendStatus(500);
    }
    res.type('json');
    if (packages.length) {
      return res.status(200).send(packages);
    }

    return res.status(200).send([]);
  });
});

app.get('/package/:package', function(req, res) {
  let packageString = req.params.package;
  if (packageString.indexOf('@') === -1) {
    packageString += '@latest';
  }

  cache.get(packageString, (cacheGetError, value) => {
    if (cacheGetError) {
      console.error(cacheGetError);
      return res.sendStatus(500);
    }
    if (value) {
      console.info(`Getting ${packageString} from cache.`);
      res.type('json');
      return res.status(200).send(value);
    }

    console.info(`Getting ${packageString} from npm registry.`);
    request.get(
      `https://registry.npmjs.org/${packageString.replace('@', '/')}`,
      (npmErr, npmResponse, body) => {
        if (npmErr) {
          console.error(npmErr);
          return res.sendStatus(502);
        }
        if (npmResponse.statusCode !== 200) {
          console.warn(`NPM registry call wasn't successful with ${body}`);
          return res.status(npmResponse.statusCode).send(body);
        }
        let packageInfo;
        try {
          packageInfo = JSON.parse(body);
        } catch (e) {
          console.error(`Unable to parse response for ${packageString}`);
          return res.sendStatus(502);
        }

        const dependencies = packageInfo.dependencies || {};
        res.status(200).send(dependencies);
        let packageName = packageString.split('@')[0];
        cache.set(
          `${packageName}@${packageInfo.version}`,
          dependencies,
          (cacheSetError, cacheSetSuccess) => {
            if (cacheSetError || !cacheSetSuccess) {
              console.error(`Caching ${packageName}@${packageInfo.version} failed.`);
              console.error(cacheSetError);
            } else {
              console.info(`${packageName}@${packageInfo.version} cached successfully.`);
            }
          }
        );
      }
    );
  });
});

app.listen(3000, function() {
  console.log('Example app listening on port 3000!');
});
